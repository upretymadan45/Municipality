package com.example.tcs.mechinagar_12.com.muninepal.tcs;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tcs.mechinagar_12.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import models.Place;

public class PlaceDetailFragment extends Fragment {

    private FirebaseDatabase database = FirebaseDatabase.getInstance();

    private ImageView imageView;
    private TextView tvDescription;

    public PlaceDetailFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_place_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        String uid = bundle.getString("uid");

        imageView = (ImageView) view.findViewById(R.id.fullImage);
        tvDescription = (TextView) view.findViewById(R.id.tvDescription);

        loadData(uid);
    }

    public void loadData(String uid){
        DatabaseReference ref = database.getReference("places/"+uid);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Place place = dataSnapshot.getValue(Place.class);
                if(place==null)
                    return;

                tvDescription.setText(place.get_description());
                if(isAdded())
                Glide.with(getContext()).load(place.get_imageUrl()).into(imageView);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
