package com.example.tcs.mechinagar_12.com.muninepal.tcs;

/**
 * Created by TOSHIBA on 4/22/2015.
 */

public class Setting_ArrayItems {
    private int imageid;
    private String titleofcity;
    private String desofcity;

    public Setting_ArrayItems(int imageid, String titleofcity, String desofcity)
    {
        this.imageid=imageid;
        this.titleofcity=titleofcity;
        this.desofcity=desofcity;
    }
    public int getImageid()
    {
        return imageid;
    }
    public void setImageid(int imageid)
    {
        this.imageid=imageid;
    }
    public String getTitleofcity()
    {
        return titleofcity;
    }
    public void setTitleofcity(String titleofcity)
    {
        this.titleofcity=titleofcity;
    }
    public String getDesofcity()
    {
        return desofcity;
    }
    public void setDesofcity(String desofcity)
    {
        this.desofcity=desofcity;
    }

}
