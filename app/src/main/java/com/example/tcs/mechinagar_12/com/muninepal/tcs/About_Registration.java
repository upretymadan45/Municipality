package com.example.tcs.mechinagar_12.com.muninepal.tcs;

/**
 * Created by TOSHIBA on 4/22/2015.
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.tcs.mechinagar_12.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TOSHIBA on 4/21/2015.
 */
public class About_Registration extends Activity implements AdapterView.OnItemClickListener {
    EditText txtolddate;
    Button btnDel_OldAtndnc;
    String returnString;
    TextView t;

    public static final String[] citiesTitle = new String[]{
            "सिफारिस सम्बन्धि :","प्रमाणित सम्बन्धि : ",
            "घर जग्गा सम्बन्धि ","दर्ता सम्बन्धि :",
            "खाने पानी तथा बिधुत सम्बन्धि","शैछिक गतिबिधि  ",
            "आर्थिक गतिबिधि ",
           };

    public static final String[] citiesDes = new String[]{
            "सबै प्रकारका सिफारिसहरु: नागरिकता, घर जग्गा , उधोग स्थापना सिफारिस आदि ","नाता प्रमाणित, जन्ममिति, बसोबास,चारकिल्ला,मिर्तु प्रमाणित आदि",
            "घरजग्गा मूल्यांकन, घर नक्सा इजाजत, मोहीको नामसारी, चारकिल्ला आदि "," जन्म, मिर्तु,, व्यसाय, उधोग दर्ता आदि",
            "खाने पानी तथा बिधुत जडान, महसुल आदि","बिद्यालय नामसरि, ठाउँ सरि, कक्षा बढाउने आदि ",
            "योजना पेस्की फर्सौट, आर्थिक अवस्था कम्जोर सिफारिस आदि",


    };

    public static final Integer[] images = {
            R.drawable.logo_reg,R.drawable.logo_reg,
            R.drawable.logo_reg,R.drawable.logo_reg,
            R.drawable.logo_reg, R.drawable.logo_reg,R.drawable.logo_reg
            };

    ListView listView;
    List<Setting_ArrayItems> rowItems;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.about_reg);


      //  txtolddate=(EditText)findViewById(R.id.edit_olddate);
      //  btnDel_OldAtndnc=(Button)findViewById(R.id.btn_olddate);

        t=(TextView)findViewById(R.id.aa_date);

        Intent intentw = getIntent();
        String messae = intentw.getStringExtra("msag");
        //  et_=(TextView)findViewById(R.id.st_date);
        t.setText(messae);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        rowItems = new ArrayList<Setting_ArrayItems>();
        for (int i = 0; i < citiesTitle.length; i++) {
            Setting_ArrayItems item = new Setting_ArrayItems(images[i], citiesTitle[i], citiesDes[i]);
            rowItems.add(item);
        }
        listView = (ListView) findViewById(R.id.frmlistviewxml);
        CustomArrayAdapter adapter = new CustomArrayAdapter(this,
                R.layout.display_class_array, rowItems);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        //getActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onItemClick(AdapterView<?> a, View v, int position, long id) {
        if (position == 0) {
            Intent i = new Intent(this,Sifaris_Activity.class);
           // i.putExtra("msag",t.getText().toString());
            startActivity(i);

            //Toast.makeText(this, "You pressed the first item in the list",
            //  Toast.LENGTH_SHORT).show();
        } /*else {
            Toast.makeText(this, "Birtamod City",
                    Toast.LENGTH_SHORT).show();*/


        else if (position == 1) {
            Intent i = new Intent(this,Pramanit_Activity.class);
           // i.putExtra("msag",t.getText().toString());
            startActivity(i);


        }
        else if (position == 2) {
            Intent i = new Intent(this, Gharjagga_Activity.class);
           // i.putExtra("msag",t.getText().toString());
            startActivity(i);


        }
        else if (position == 3) {
            Intent i = new Intent(this, Darta_Activity.class);

          //  i.putExtra("msag",t.getText().toString());
            startActivity(i);


        }
        else if (position == 4) {
            Intent i = new Intent(this, Khanepani_bidhut_Activity.class);

            //  i.putExtra("msag",t.getText().toString());
            startActivity(i);
        }
        else if (position == 5) {
            Intent i = new Intent(this, Saichik_activity.class);

            //  i.putExtra("msag",t.getText().toString());
            startActivity(i);
        }
        else if (position == 6) {
            Intent i = new Intent(this, Aarthik_activity.class);

            //  i.putExtra("msag",t.getText().toString());
            startActivity(i);
        }







    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                finish();
                //Intent parentIntent1 = new Intent(this,MainActivity.class);
                //startActivity(parentIntent1);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
