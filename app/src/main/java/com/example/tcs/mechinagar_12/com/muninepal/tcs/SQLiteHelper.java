package com.example.tcs.mechinagar_12.com.muninepal.tcs;

/**
 * Created by Juned on 1/23/2017.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {

    static String DATABASE_NAME="SubjectDataBase";

    public static final String TABLE_NAME="tblnotice";

    public static final String Table_Column_ID="id";
    public static final String Table_Column_date="datentime";

    public static final String Table_Column_1_Subject_Name="subofnotice";

    public static final String Table_Column_2_SubjectFullForm="desofnotice";

    public SQLiteHelper(Context context) {

        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        String CREATE_TABLE="CREATE TABLE IF NOT EXISTS "+TABLE_NAME+" ("+Table_Column_ID+" INTEGER PRIMARY KEY, "+Table_Column_date+" VARCHAR, "+Table_Column_1_Subject_Name+" VARCHAR, "+Table_Column_2_SubjectFullForm+" VARCHAR)";
        database.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);

    }

}