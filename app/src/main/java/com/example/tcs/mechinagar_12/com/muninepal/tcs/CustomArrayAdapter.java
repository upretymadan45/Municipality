package com.example.tcs.mechinagar_12.com.muninepal.tcs;

/**
 * Created by TOSHIBA on 4/22/2015.
 */


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tcs.mechinagar_12.R;

import java.util.List;

/**
 * Created by TOSHIBA on 4/21/2015.
 */
public class CustomArrayAdapter extends ArrayAdapter<Setting_ArrayItems> {

    Context context;
    public CustomArrayAdapter(Context context, int resourceId, List<Setting_ArrayItems> items)
    {
        super(context,resourceId,items);
        this.context=context;
    }
    private class ViewHolder
    {
        ImageView imgview;
        TextView title,desc;
    }
    public View getView(int position, View view1, ViewGroup parent)
    {
        ViewHolder holder=null;
        Setting_ArrayItems setcityname=getItem(position);
        LayoutInflater inflat=(LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if(view1==null)
        {

            view1=inflat.inflate(R.layout.display_class_array, null);
            holder=new ViewHolder();
            holder.title=(TextView)view1.findViewById(R.id.school_class);
            holder.desc=(TextView)view1.findViewById(R.id.school_sec);
            holder.imgview = (ImageView) view1.findViewById(R.id.icon1);
            view1.setTag(holder);
        }
        else
            holder = (ViewHolder) view1.getTag();

        holder.title.setText(setcityname.getTitleofcity());
        holder.desc.setText(setcityname.getDesofcity());
        holder.imgview.setImageResource(setcityname.getImageid());
        return view1;

    }
}
