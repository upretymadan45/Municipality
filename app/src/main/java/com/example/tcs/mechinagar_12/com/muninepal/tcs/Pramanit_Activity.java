package com.example.tcs.mechinagar_12.com.muninepal.tcs;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.tcs.mechinagar_12.R;

public class Pramanit_Activity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pramanit_activity);
		//getActionBar().setDisplayHomeAsUpEnabled(true);
//		getActionBar().hide();
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbari);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbari);
		collapsingToolbar.setTitle("नागरिक वडापत्र");
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				// app icon in action bar clicked; goto parent activity.
				finish();
				//Intent parentIntent1 = new Intent(this,MainActivity.class);
				//startActivity(parentIntent1);

				return true;



			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
