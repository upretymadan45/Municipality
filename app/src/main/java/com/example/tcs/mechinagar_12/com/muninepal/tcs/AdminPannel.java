package com.example.tcs.mechinagar_12.com.muninepal.tcs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tcs.mechinagar_12.R;

/**
 * Created by TOSHIBA on 8/5/2015.
 */
public class AdminPannel extends Activity {
    TextView t;
    public AdminPannel()
    {

    }
    final Context context = this;
Intent intent1;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.adminpannel);

//       getActionBar().setDisplayHomeAsUpEnabled(true);



       // intent1 = new Intent(AdminPannel.this,Admin_Staff.class);
        final ImageView img = (ImageView)findViewById(R.id.icoattendance);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.prompts, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);


                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);
               // alertDialogBuilder.setIcon(R.drawable.icoschool);
               // alertDialogBuilder.setTitle("Security Code !");

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.edittxthrsec);




                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("send",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        final String hrsec= userInput.getText().toString();

                                        if(hrsec.equals("a"))
                                        {
                                            Intent intenth=new Intent(AdminPannel.this, Admin_Staff.class);
                                            intenth.putExtra("messag",t.getText().toString());
                                            startActivity(intenth);

                                        }
                                        else
                                        {
                                            // Toast.makeText(getApplicationContext(), "Security Problem !!", Toast.LENGTH_SHORT).show();
                                            // return ;
                                            showAlertDialog(AdminPannel.this, "Security Problem !!",
                                                    "aren't you authorized person ??", false);
                                            Toast.makeText(getApplicationContext(), "reporting to database!!", Toast.LENGTH_SHORT).show();
                                            return ;

                                        }

                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
alertDialog.setIcon(R.drawable.logo_reg);
                alertDialog.setTitle("Security Code !");
                // show it

                alertDialog.show();

                //startActivity(intent1);

            }
        });


        // intent1 = new Intent(AdminPannel.this,Admin_Staff.class);
        final ImageView img1 = (ImageView)findViewById(R.id.icoentrance);
        img1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.prompts,null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.edittxthrsec);




                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("send",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        final String hrsec= userInput.getText().toString();

                                        if(hrsec.equals("a"))
                                        {
                                            Intent intenth=new Intent(AdminPannel.this, Admin_principal.class);
                                            intenth.putExtra("messag",t.getText().toString());
                                            startActivity(intenth);

                                        }
                                        else
                                        {
                                            // Toast.makeText(getApplicationContext(), "Security Problem !!", Toast.LENGTH_SHORT).show();
                                            // return ;
                                            showAlertDialog(AdminPannel.this, "Security Problem !!",
                                                    "aren't you authorized person ??", false);
                                            Toast.makeText(getApplicationContext(), "reporting to database!!", Toast.LENGTH_SHORT).show();
                                            return ;

                                        }

                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.setIcon(R.drawable.logo_reg);
                alertDialog.setTitle("Security Code !");
                alertDialog.show();


                //startActivity(intent1);

            }
        });

        Intent intent = getIntent();
        String messae = intent.getStringExtra("messag");
        t=(TextView)findViewById(R.id.txtd);
       t.setText(messae);
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon(R.drawable.logo_reg);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                finish();
                //Intent parentIntent1 = new Intent(this,MainActivity.class);
                //startActivity(parentIntent1);

                return true;



            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
