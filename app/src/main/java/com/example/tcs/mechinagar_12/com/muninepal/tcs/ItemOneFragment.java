/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.example.tcs.mechinagar_12.com.muninepal.tcs;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tcs.mechinagar_12.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import adapters.ImageListAdapter;
import interfaces.IPlaceClickNotifier;
import models.Place;

public class ItemOneFragment extends Fragment implements IPlaceClickNotifier{
    private static final String TAG = ItemOneFragment.class.getSimpleName();

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private IPlaceClickNotifier notifier;
    private List<Place> places = new ArrayList<Place>();
    private RecyclerView rvImageList;

   public ItemOneFragment()
    {

    }
    @Override
    public void onStart() {
        super.onStart();
    }

    public static ItemOneFragment newInstance() {
        ItemOneFragment fragment = new ItemOneFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_item_one, container, false);

       //CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) v.findViewById(R.id.collapsing_toolbar2);
        //collapsingToolbar.setTitle("Welcome");

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        notifier = this;
        rvImageList = (RecyclerView) view.findViewById(R.id.rvImageList);

        loadImageList();
    }

    @Override
    public void notify(String uid) {
        Bundle bundle = new Bundle();
        bundle.putString("uid",uid);
        PlaceDetailFragment placeDetailFragment = new PlaceDetailFragment();
        placeDetailFragment.setArguments(bundle);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction
                .setCustomAnimations(
                R.animator.card_flip_right_in,
                R.animator.card_flip_right_out,
                R.animator.card_flip_left_in,
                R.animator.card_flip_left_out);

        transaction.addToBackStack("");

        transaction.replace(R.id.frame_layout,placeDetailFragment,"");
        transaction.commit();
    }

    @Override
    public void notifyShowMap(String locationName) {
            String url = "geo:0,0?q="+locationName +", Nepal";
            Uri gmmIntentUri = Uri.parse(url);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
    }

    public void loadImageList(){
        DatabaseReference ref = database.getReference("places");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                places.clear();
                if(dataSnapshot.getValue()!=null)
                for (DataSnapshot snapshot:dataSnapshot.getChildren()) {
                        Place place = (Place)snapshot.getValue(Place.class);
                        place.set_uid(snapshot.getKey());
                        places.add(place);
                }

                ImageListAdapter adapter = new ImageListAdapter(getActivity(),places,notifier);
                rvImageList.setAdapter(adapter);
                rvImageList.setLayoutManager(new LinearLayoutManager(getActivity()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
