package com.example.tcs.mechinagar_12.com.muninepal.tcs;

/**
 * Created by 21502476 on 24/11/2015.
 */
public class DateFetcher {
    public String englishDate;
    public String nepaliDate;
    public DateFetcher()
    {

    }
    public DateFetcher(String engDate, String nepDate)
    {
        this.englishDate=engDate;
        this.nepaliDate=nepDate;
    }
}
