/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.example.tcs.mechinagar_12.com.muninepal.tcs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.tcs.mechinagar_12.R;

public class ItemTwoFragment extends Fragment {
    Intent intent1,intent2,intent3;
    public static ItemTwoFragment newInstance() {
        ItemTwoFragment fragment = new ItemTwoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_item_two, container, false);

        intent1 = new Intent(getActivity(), About_Registration.class);
        final ImageView imgal = (ImageView) v.findViewById(R.id.icoentra);
        imgal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(intent1);


            }
        });
        intent2 = new Intent(getActivity(), Darta_Activity.class);
        final ImageView imgalr = (ImageView) v.findViewById(R.id.ictra);
        imgalr.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(intent2);
            }
        });

        intent3 = new Intent(getActivity(), Notice_View_Primary.class);
        final ImageView imgalrr = (ImageView) v.findViewById(R.id.icoence);
        imgalrr.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(intent3);
            }
        });
        return v;
    }
    }

