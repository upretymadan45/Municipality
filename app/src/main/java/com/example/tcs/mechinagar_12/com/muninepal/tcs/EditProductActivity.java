package com.example.tcs.mechinagar_12.com.muninepal.tcs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tcs.mechinagar_12.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EditProductActivity extends AppCompatActivity {
    TextView Viewpid1;
	EditText n_date;
	EditText n_sub;
	EditText n_des;

	Button btnSave;
	Button btnDelete;
	String pid;
	// Progress Dialog
	private ProgressDialog pDialog;

	// JSON parser class
	JSONParser jsonParser = new JSONParser();

	// single product url
	//private static final String url_product_details = "http://androiddatabase.net16.net/android_connect/get_product_details.php";

	// url to update product
	private static final String url_update_product = "http://sigmaitsolution.ipage.com/siliconvelly/android_amity/update_notice.php";
	
	// url to delete product
	private static final String url_delete_product = "http://sigmaitsolution.ipage.com/siliconvelly/android_amity/delete_notice.php";

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_PRODUCT = "product";
	private static final String TAG_PID = "id";
	private static final String TAG_NAME = "datentime";
	private static final String TAG_PRICE = "subofnotice";
	private static final String TAG_DESCRIPTION = "desofnotice";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_product);

       // getActionBar().setDisplayHomeAsUpEnabled(true);
        Viewpid1 = (TextView) findViewById(R.id.inputPid);
        n_date = (EditText) findViewById(R.id.inpName);
        n_sub = (EditText) findViewById(R.id.inpPrice);
        n_des = (EditText) findViewById(R.id.inpDesc);



        Intent intent1 = getIntent();
        String str1 = intent1.getStringExtra("location1");
        String str2 = intent1.getStringExtra("location2");
        String str3 = intent1.getStringExtra("location3");
        String str4 = intent1.getStringExtra("location4");

       Viewpid1.setText(str1);
        n_date.setText(str2);
        n_sub.setText(str3);
        n_des.setText(str4);

		// save button
		btnSave = (Button) findViewById(R.id.btnSave);
		btnDelete = (Button) findViewById(R.id.btnDelete);

		// getting product details from intent
		/*Intent ie = getIntent();
		
		// getting product id (pid) from intent
		pid = ie.getStringExtra(TAG_PID);

		// Getting complete product details in background thread
		new GetProductDetails().execute();

		// save button click event*/
		btnSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// starting background task to update product
				new SaveProductDetails().execute();
			}
		});

		// Delete button click event
		btnDelete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// deleting product in background thread
				new DeleteProduct().execute();
			}
		});

	}


	 //Background Async Task to Get complete product details

	/**class GetProductDetails extends AsyncTask<String, String, String> {


		 // Before starting background thread Show Progress Dialog

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EditProductActivity.this);
			pDialog.setMessage("Loading product details. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * Getting product details in background thread
		 * */
		/*protected String doInBackground(String... params) {

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					// Check for success tag
					int success;
					try {
						// Building Parameters
						List<NameValuePair> params = new ArrayList<>();
						params.add(new BasicNameValuePair("pid", pid));

						// getting product details by making HTTP request
						// Note that product details url will use GET request
						JSONObject json = jsonParser.makeHttpRequest(url_product_details, "GET", params);

						// check your log for json response
						Log.d("Single Product Details", json.toString());
						
						// json success tag
						success = json.getInt(TAG_SUCCESS);
						if (success == 1) {
							// successfully received product details
							JSONArray productObj = json
									.getJSONArray(TAG_PRODUCT); // JSON Array
							
							// get first product object from JSON Array
							JSONObject product = productObj.getJSONObject(0);

							// product with this pid found
							// Edit Text
							txtName = (EditText) findViewById(R.id.inpName);
							txtPrice = (EditText) findViewById(R.id.inpPrice);
							txtDesc = (EditText) findViewById(R.id.inpDesc);

							// display product data in EditText
							txtName.setText(product.getString(TAG_NAME));
							txtPrice.setText(product.getString(TAG_PRICE));
							txtDesc.setText(product.getString(TAG_DESCRIPTION));

						}else{
							// product with pid not found
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});

			return null;
		}


		// After completing background task Dismiss the progress dialog

		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			pDialog.dismiss();
		}
	}*/

	/**
	 * Background Async Task to  Save product Details
	 * */
	class SaveProductDetails extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EditProductActivity.this);
			pDialog.setMessage("Saving notice ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();

            Toast.makeText(getApplicationContext(), "data successfully updated", Toast.LENGTH_SHORT).show();
		}

		/**
		 * Saving product
		 * */
		protected String doInBackground(String... args) {
        // getting updated data from EditTexts
            String str1 = Viewpid1.getText().toString();
            String str2 = n_date.getText().toString();
            String str3 = n_sub.getText().toString();
            String str4 = n_des.getText().toString();


			/*String name = inpName.getText().toString();
			String price = txtPrice.getText().toString();
			String description = txtDesc.getText().toString();*/

			// Building Parameters
			List<NameValuePair> params = new ArrayList<>();
			params.add(new BasicNameValuePair(TAG_PID, str1));
			params.add(new BasicNameValuePair(TAG_NAME, str2));
			params.add(new BasicNameValuePair(TAG_PRICE, str3));
			params.add(new BasicNameValuePair(TAG_DESCRIPTION, str4));

			// sending modified data through http request
			// Notice that update product url accepts POST method
			JSONObject json = jsonParser.makeHttpRequest(url_update_product,
					"POST", params);

			// check json success tag
			try {
				int success = json.getInt(TAG_SUCCESS);
				
				if (success == 1) {
					// successfully updated
					Intent ie = getIntent();
					// send result code 100 to notify about product update
					setResult(100, ie);
					finish();
				} else {
					// failed to update product
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product uupdated
			pDialog.dismiss();
		}
	}

	/*****************************************************************
	 * Background Async Task to Delete Product
	 * */
	class DeleteProduct extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EditProductActivity.this);
			pDialog.setMessage("Deleting Product...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();

            Toast.makeText(getApplicationContext(),"data successfully deleted", Toast.LENGTH_SHORT).show();
		}

		/**
		 * Deleting product
		 * */
		protected String doInBackground(String... args) {

			// Check for success tag
			int success;
			try {
				// Building Parameters
				List<NameValuePair> params = new ArrayList<NameValuePair>();
                String id = Viewpid1.getText().toString();


			params.add(new BasicNameValuePair("id",id ));

				// getting product details by making HTTP request
				JSONObject json = jsonParser.makeHttpRequest(
						url_delete_product, "POST", params);

				// check your log for json response
				Log.d("Delete Product", json.toString());
				
				// json success tag
				success = json.getInt(TAG_SUCCESS);
				if (success == 1) {
					// product successfully deleted
					// notify previous activity by sending code 100
					Intent ie = getIntent();
					// send result code 100 to notify about product deletion
					setResult(100, ie);
					finish();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted
			pDialog.dismiss();

		}

	}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                finish();
                //Intent parentIntent1 = new Intent(this,MainActivity.class);
                //startActivity(parentIntent1);

                return true;



            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
