package com.example.tcs.mechinagar_12.com.muninepal.tcs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tcs.mechinagar_12.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Notice_View_Primary extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
   // Boolean isInternetPresent = false;
   // ConnectionDetector cd;
   // Intent intent1,intent2;
  private FloatingActionButton fab;
    SQLiteDatabase sqLiteDatabase;

    Button ShowSQLiteDataInListView;

    String HttpJSonURL = "http://ashokbhetwal.com.np/mechinagar12/SubjectFullForm.php";


    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notice_view_primary);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       // SaveButtonInSQLite = (Button)findViewById(R.id.button);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        ShowSQLiteDataInListView = (Button)findViewById(R.id.button2);



       /* SaveButtonInSQLite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            //    SQLiteDataBaseBuild();

              //  SQLiteTableBuild();

               // DeletePreviousData();

               // new StoreJSonDataInToSQLiteClass(MainActivity.this).execute();
            }
        });*/

        ShowSQLiteDataInListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              //  Intent intent = new Intent(MainActivity.this, ShowDataActivity.class);
               // startActivity(intent);
                checkConnection();

            }
        });


    }
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;

            SQLiteDataBaseBuild();
            SQLiteTableBuild();
            DeletePreviousData();
            new StoreJSonDataInToSQLiteClass(Notice_View_Primary.this).execute();



        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
            Intent intent3=new Intent(Notice_View_Primary.this,Notice_View_Seondary.class);
            startActivity(intent3);
        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.fab), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }
    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }



    /**
     * Callback will be triggered when there is change in
     * network connection
     */


     /*   public void checkInternet()
        {
            // get Internet status
            isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            SQLiteDataBaseBuild();

            SQLiteTableBuild();

            DeletePreviousData();

            new StoreJSonDataInToSQLiteClass(MainActivity.this).execute();

          //  intent1 = new Intent(this,MainActivity.class);
           // startActivity(intent1);
            // Intent intentmain=new Intent(Leave.this,MainActivity.class);
            // startActivity(intentmain);

//this.finish();
            // Internet Connection is Present
            // make HTTP requests
            //showAlertDialog(AndroidDetectInternetConnectionActivity.this, "Internet Connection",
            //"You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            intent2 = new Intent(this,ShowDataActivity.class);
            startActivity(intent2);

            return ;
        }

    }*/

    private class StoreJSonDataInToSQLiteClass extends AsyncTask<Void, Void, Void> {

        public Context context;

        String FinalJSonResult;

        public StoreJSonDataInToSQLiteClass(Context context) {

            this.context = context;
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            progressDialog = new ProgressDialog(Notice_View_Primary.this);
            progressDialog.setTitle("सुचना स्टोर हुदैछ l");
            progressDialog.setMessage("कृपया पर्खनुहोस..");
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            HttpServiceClass httpServiceClass = new HttpServiceClass(HttpJSonURL);

            try {
                httpServiceClass.ExecutePostRequest();

                if (httpServiceClass.getResponseCode() == 200) {

                    FinalJSonResult = httpServiceClass.getResponse();

                    if (FinalJSonResult != null) {

                        JSONArray jsonArray = null;
                        try {

                            jsonArray = new JSONArray(FinalJSonResult);
                            JSONObject jsonObject;

                            for (int i = 0; i < jsonArray.length(); i++) {

                                jsonObject = jsonArray.getJSONObject(i);
                                //added
                                String datentime = jsonObject.getString("datentime");

                                String subofnotice = jsonObject.getString("subofnotice");

                                String desofnotice = jsonObject.getString("desofnotice");

                                String SQLiteDataBaseQueryHolder = "INSERT INTO "+SQLiteHelper.TABLE_NAME+"(datentime,subofnotice,desofnotice) VALUES('"+datentime+"','"+subofnotice+"', '"+desofnotice+"');";

                                sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);

                            }
                            Intent intent4=new Intent(Notice_View_Primary.this,Notice_View_Seondary.class);
                            startActivity(intent4);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                } else {

                    Toast.makeText(context, httpServiceClass.getErrorMessage(), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)

        {
            sqLiteDatabase.close();

            progressDialog.dismiss();

           Toast.makeText(Notice_View_Primary.this,"लोड सफल भयो l", Toast.LENGTH_LONG).show();

        }
    }


    public void SQLiteDataBaseBuild(){

        sqLiteDatabase = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);

    }

    public void SQLiteTableBuild(){

        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS "+SQLiteHelper.TABLE_NAME+" ("+SQLiteHelper.Table_Column_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+SQLiteHelper.Table_Column_date+" VARCHAR, "+SQLiteHelper.Table_Column_1_Subject_Name+" VARCHAR, "+SQLiteHelper.Table_Column_2_SubjectFullForm+" VARCHAR);");

    }

    public void DeletePreviousData(){

        sqLiteDatabase.execSQL("DELETE FROM "+SQLiteHelper.TABLE_NAME+"");

    }


}
