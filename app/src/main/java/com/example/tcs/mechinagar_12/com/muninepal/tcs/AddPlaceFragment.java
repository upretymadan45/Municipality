package com.example.tcs.mechinagar_12.com.muninepal.tcs;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.tcs.mechinagar_12.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import helpers.StorageHelper;
import models.Place;

public class AddPlaceFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = AddPlaceFragment.class.getSimpleName();
    private final int PICK_PHOTO_FOR_AVATAR = 1;
    EditText placeName, description;
    ImageButton upload;
    Button save;
    private String placeImageUrl;

    private StorageReference mStorageRef;

    public AddPlaceFragment() {

    }

    public static AddPlaceFragment getAddPlaceFragmentInstance(){
        return new AddPlaceFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_place, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mStorageRef = FirebaseStorage.getInstance().getReference();

        placeName = (EditText) view.findViewById(R.id.etPlaceName);
        description = (EditText) view.findViewById(R.id.etDescription);
        upload = (ImageButton) view.findViewById(R.id.uploadImage);
        save = (Button) view.findViewById(R.id.save);

        upload.setOnClickListener(this);
        save.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == save){
            if(placeName.getText().toString().isEmpty()){
                placeName.setError("Required");
                return;
            }

            if(description.getText().toString().isEmpty()){
                description.setError("Required");
                return;
            }

            Place place = new Place();
            place.set_name(placeName.getText().toString());
            place.set_description(placeName.getText().toString());
            place.set_imageUrl(placeImageUrl);
            place.set_postedDate(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));

            new StorageHelper().store(place);
        }

        if(v==upload){
            pickImage();
        }
    }

    public void pickImage() {
        placeImageUrl = "";
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == getActivity().RESULT_OK){
            if(data==null){
                return;
            }

            Log.d(TAG, "onActivityResult: called");
            Log.d(TAG, "onActivityResult: "+data.getData());

            try{
                InputStream inputStream = getContext().getContentResolver().openInputStream(data.getData());
                Log.d(TAG, "onActivityResult: "+inputStream);
                DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
                String date = dateFormat.format(new Date());

                StorageReference storageref = mStorageRef.child("places").child(date+".jpg");
                storageref.putStream(inputStream)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                placeImageUrl = taskSnapshot.getDownloadUrl().toString();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        })
                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                Toast.makeText(getContext(),"Uploading.please wait..",Toast.LENGTH_LONG).show();
                            }
                        });

            } catch(FileNotFoundException ex){
                Log.d(TAG, "onActivityResult: "+ex);
            }
        }
    }
}
