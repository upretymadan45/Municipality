package com.example.tcs.mechinagar_12.com.muninepal.tcs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tcs.mechinagar_12.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TOSHIBA on 7/30/2015.
 */
public class Notice_Add extends Activity {
    ArrayAdapter<String> adapter;
   // Intent intent1;
    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    JSONParser jsonParser = new JSONParser();



    EditText edit_datetime;
    EditText edit_subjectofnotice;
    EditText edit_desofnotice;
    //

    // url to create new product
    private static String url_create_product = "http://ashokbhetwal.com.np/mechinagar12/add_notice.php";
    private ProgressDialog pDialog;
    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private String _noticeSub = "";

    @Override
    protected void onCreate(Bundle b)
    {
        super.onCreate(b);
        setContentView(R.layout.notice_add);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
      //getActionBar().setDisplayHomeAsUpEnabled(true);
        cd = new ConnectionDetector(getApplicationContext());
        // spinner1
       /* final Spinner spin1 = (Spinner)findViewById(R.id.stafcode);

        String url1 = "http://jbsbrt.site90.net/jbs/combo_jbsBranch.php";

        try {

            JSONArray data = new JSONArray(getJSONUrl(url1));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for(int i = 0; i < data.length(); i++){
                JSONObject c = data.getJSONObject(i);

                map = new HashMap<String, String>();
                // map.put("customerID", c.getString("customerID"));
                map.put("Name", c.getString("Name"));
                //map.put("phone", c.getString("phone"));
                MyArrList.add(map);

            }
            SimpleAdapter sAdap1;
            sAdap1 = new SimpleAdapter(Leave.this, MyArrList, R.layout.combo_jbsbranch,
                    new String[] {"Name"}, new int[] {R.id.jbsbranch,});
            spin1.setAdapter(sAdap1);

            //  final AlertDialog.Builder viewDetail = new AlertDialog.Builder(this);



        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
        Button btnAddingProducts = (Button) findViewById(R.id.btn_addnotice);



        //stfcode = (Spinner) findViewById(R.id.stafcode);
       // stname = (EditText) findViewById(R.id.sname);
        edit_datetime = (EditText) findViewById(R.id.datetime);
        edit_subjectofnotice = (EditText)findViewById(R.id.sub_notice);
        edit_desofnotice = (EditText) findViewById(R.id.desofnotice);

        // btnAddProduct.setOnClickListener(new View.OnClickListener() {
        // button click event
        btnAddingProducts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // creating new product in background thread
                //String amt1= brncode.getText().toString();
                //if(TextUtils.isEmpty(amt1)) {
                  //  brncode.setError("Leave Type ");
                   // return;
               // }
               // String amt2= stfcode.getText().toString();
               // if(TextUtils.isEmpty(amt2)) {
                    //stfcode.setError("Branch Name");
                    //return;
               // }


                String a= edit_datetime.getText().toString();
                if(TextUtils.isEmpty(a)) {
                    edit_datetime.setError("Date");
                    return;
                }
                String ab= edit_subjectofnotice.getText().toString();
                if(TextUtils.isEmpty(ab)) {
                    edit_subjectofnotice.setError("Subject of Notice");
                    return;
                }
                String abc= edit_desofnotice.getText().toString();
                if(TextUtils.isEmpty(abc)) {
                    edit_desofnotice.setError("Notice Details");
                    return;
                }
                if( a.length()==0 | ab.length()==0 | abc.length()==0)
                {

                    Toast.makeText(getApplicationContext(),"all fields required !", Toast.LENGTH_SHORT).show();
                    Intent intent4= new Intent(Notice_Add.this,Notice_Add.class);
                    startActivity(intent4);
                }

                else {
                    checkInternet();
                   // new CreateNewProduct().execute();
                }

                //  new CreateNewProduct().execute();


            }
        });
     /*   Button clear=(Button) findViewById(R.id.btnclear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edit_datetime.setText("");
                edit_subjectofnotice.setText("");
                edit_desofnotice.setText("");



            }

        });*/
       // String date1 = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
       // edit_datetime.setText(date1, TextView.BufferType.EDITABLE);

        Intent intent = getIntent();
        String messae = intent.getStringExtra("mssag");
        edit_datetime.setText(messae);
        //t=(TextView)findViewById(R.id.ddd);
        //t.setText(messae);

        Button d = (Button) findViewById(R.id.btnviewnotice_admin);
        d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet1();

            }
        });




    }

  /* This code is for combo box from php
   public String getJSONUrl(String url) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return str.toString();
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    public void checkInternet()
    {
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            new CreateNewProduct().execute();

           // Intent intentmain=new Intent(Leave.this,MainActivity.class);
           // startActivity(intentmain);

//this.finish();
            // Internet Connection is Present
            // make HTTP requests
            //showAlertDialog(AndroidDetectInternetConnectionActivity.this, "Internet Connection",
            //"You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            showAlertDialog(Notice_Add.this, "No Internet Connection",
                    "Please Check Internet.", false);
            return ;
        }
    }
    public void checkInternet1()
    {
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();
/*
        // check for Internet status
        if (isInternetPresent) {
           // new CreateNewProduct().execute();
            Intent intent = new Intent(Notice_Add.this, Admin_viewnotice.class);
            startActivity(intent);
            // Intent intentmain=new Intent(Leave.this,MainActivity.class);
            // startActivity(intentmain);

//this.finish();
            // Internet Connection is Present
            // make HTTP requests
            //showAlertDialog(AndroidDetectInternetConnectionActivity.this, "Internet Connection",
            //"You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            showAlertDialog(Notice_Add.this, "No Internet Connection",
                    "Please Check Internet.", false);
            return ;
        }*/
    }


    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     * */
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    class CreateNewProduct extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Notice_Add.this);
            pDialog.setMessage("Publishing Notice...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

            Toast.makeText(getApplicationContext(), "data successfully sent", Toast.LENGTH_SHORT).show();


        }

        /**
         * Creating product
         * */
        protected String doInBackground(String...   args) {




           // String branchId= stfcode.getSelectedItem().toString();
           // String datentime = edit_datetime.getText().append("\u23FA").toString();
            String datentime = edit_datetime.getText().toString();
            String subofnotice = edit_subjectofnotice.getText().toString();
            String desofnotice = edit_desofnotice.getText().toString();
           // String totalDays = lvtype.getText().toString();



            // Building Parameters
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("datentime", datentime));
           // params.add(new BasicNameValuePair("branchId", branchId));
           // params.add(new BasicNameValuePair("staffName", staffName));
            params.add(new BasicNameValuePair("subofnotice", subofnotice));
            params.add(new BasicNameValuePair("desofnotice", desofnotice));
           /// params.add(new BasicNameValuePair("totalDays", totalDays));

            _noticeSub = subofnotice;

            // getting JSON Object
            // Note that create product url accepts POST method



            JSONObject json = jsonParser.makeHttpRequest(url_create_product,"POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully created product

                   // Intent i = new Intent(getApplicationContext(), Notice_Add.class);
                    finish();
                   // startActivity(i);


                } else {
                    // failed to create product
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            sendNotification();
            pDialog.dismiss();


        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                finish();
               // Intent parentIntent1 = new Intent(this,MainActivity.class);
               // startActivity(parentIntent1);

                return true;



            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sendNotification(){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        ref.child("notice")
                .child("newNotice")
                .setValue(_noticeSub);
    }

}
