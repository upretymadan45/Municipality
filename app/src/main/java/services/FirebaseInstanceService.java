package services;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FirebaseInstanceService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {

        SharedPreferences preferences = this.getSharedPreferences("phonePreference", Context.MODE_PRIVATE);
        String phoneNumber = preferences.getString("number","");

        String token = FirebaseInstanceId.getInstance().getToken();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        ref.child("subscribers")
                .child(phoneNumber)
                .child("token")
                .setValue(token);
    }
}
