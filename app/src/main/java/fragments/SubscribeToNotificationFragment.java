package fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.tcs.mechinagar_12.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;


public class SubscribeToNotificationFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = SubscribeToNotificationFragment.class.getSimpleName();
    private EditText phoneNumber;
    private Button btnSubscribe;

    public SubscribeToNotificationFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_subscribe_to_notification, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        phoneNumber = (EditText) view.findViewById(R.id.phoneNumber);
        btnSubscribe = (Button) view.findViewById(R.id.btnSubscribe);
        btnSubscribe.setOnClickListener(this);
    }

    public static Fragment getFragmentInstace(){
        return new SubscribeToNotificationFragment();
    }

    @Override
    public void onClick(View v) {
        if(phoneNumber.getText().toString().equals(""))
        {
            phoneNumber.setError("Can't submit blank number");
            return;
        }

        SharedPreferences preferences = getActivity().getSharedPreferences("phonePreference",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("number",phoneNumber.getText().toString());
        editor.commit();

        String token = FirebaseInstanceId.getInstance().getToken();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        ref.child("subscribers")
                .child("token")
                .child(phoneNumber.getText().toString())
                .setValue(token);
    }
}
