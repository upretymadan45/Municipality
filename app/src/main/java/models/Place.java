package models;

/**
 * Created by madanuprety on 7/11/17.
 */

public class Place {
    private String _uid;
    private String _name;
    private String _description;
    private String _postedDate;
    private String _imageUrl;

    public Place(){

    }

    public String get_uid() {
        return _uid;
    }

    public void set_uid(String _uid) {
        this._uid = _uid;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_postedDate() {
        return _postedDate;
    }

    public void set_postedDate(String _postedDate) {
        this._postedDate = _postedDate;
    }

    public String get_imageUrl() {
        return _imageUrl;
    }

    public void set_imageUrl(String _imageUrl) {
        this._imageUrl = _imageUrl;
    }
}
