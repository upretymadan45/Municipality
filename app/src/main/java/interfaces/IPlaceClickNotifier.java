package interfaces;

/**
 * Created by madanuprety on 7/11/17.
 */

public interface IPlaceClickNotifier {
    void notify(String uid);
    void notifyShowMap(String locationName);
}
