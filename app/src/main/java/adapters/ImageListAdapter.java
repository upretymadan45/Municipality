package adapters;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.example.tcs.mechinagar_12.R;

import java.util.List;

import animations.AnimationUtils;
import interfaces.IPlaceClickNotifier;
import models.Place;

/**
 * Created by madanuprety on 7/11/17.
 */

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ViewHolder> {

    private List<Place> _places;
    private Context _context;
    private IPlaceClickNotifier _notifier;

    private int previousPosition = 0;

    public ImageListAdapter(Context context, List<Place> places, IPlaceClickNotifier notifier) {
        this._context = context;
        this._places = places;
        this._notifier = notifier;
    }

    public Context getContext(){
        return this._context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View placeListView = inflater.inflate(R.layout.image_list_row,parent,false);

        ViewHolder viewHolder = new ViewHolder(placeListView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Place place = _places.get(position);

        ImageView imageView = holder.imageView;
        TextView name = holder.placeName;

        name.setText(place.get_name());

        Glide
                .with(_context)
                .load(place.get_imageUrl())
                .into(imageView);

        if (position > previousPosition){
            AnimationUtils.slidingWithBounce(holder,true);
        }
        else {
            AnimationUtils.slidingWithBounce(holder, false);
        }
        previousPosition = position;
    }

    @Override
    public int getItemCount() {
        return _places.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public ImageView imageView;
        public TextView placeName;
        public ImageButton showMap;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.imageDisplay);

            placeName = (TextView) itemView.findViewById(R.id.placeName);

            showMap = (ImageButton) itemView.findViewById(R.id.showInMap);

            showMap.setOnClickListener(this);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();

            Place place = _places.get(position);

            if(v ==  showMap){
                _notifier.notifyShowMap(place.get_name());
            } else {
                if(position!=RecyclerView.NO_POSITION)
                    _notifier.notify(place.get_uid());
            }
        }
    }
}
