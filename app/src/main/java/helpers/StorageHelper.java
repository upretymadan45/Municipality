package helpers;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import models.Place;

/**
 * Created by madanuprety on 7/11/17.
 */

public class StorageHelper {

    private FirebaseDatabase database = FirebaseDatabase.getInstance();

    public void store(Place place){
        DatabaseReference reference = database.getReference();
        reference.child("places").push().setValue(place);
    }

}
