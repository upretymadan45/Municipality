let functions = require('firebase-functions');

let admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

exports.sendNotification = functions.database.ref('/notice/newNotice').onWrite(event => {

    return admin.database().ref('/subscribers/token').once('value').then(snap=>{
        snap.forEach(childSnapshot => {
            var token = childSnapshot.val();
            console.log("token is :"+token);

            const payload = {
                data: {
                    data_type: "direct_message",
                    title: "New Message from app",
                    message: event.data.val(),
                    message_id: "1",
                }
            };
    
            return admin.messaging().sendToDevice(token, payload)
                .then(function(response) {
                    console.log("Successfully sent message:", response);
                })
              .catch(function(error) {
                    console.log("Error sending message:", error);
              });

        });
    });
});

